const express = require('express');
const mysql = require('mysql2');
const cors = require('cors');

const app = express();
const port = 3001;

// Use CORS middleware
app.use(cors());

const db = mysql.createConnection({
    host: 'db',
    user: 'testuser',
    password: 'testpassword',
    database: 'app_data',
});

db.connect(err => {
    if (err) {
        console.error('Error connecting to MySQL:', err);
        return;
    }
    console.log('Connected to MySQL');
});

app.get('/', (req, res) => {
    db.query('SELECT text FROM messages WHERE id = 1', (err, results) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.send(results[0].text);
    });
});

app.listen(port, () => {
    console.log(`Server running on http://localhost:${port}`);
});