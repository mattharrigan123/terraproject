import React from 'react';
import {DiscordButton} from '../components/discord_comps';
import {PermModal} from '../components/modal';
import '../css/theme.css';
import '../css/login.css';

function Login () {
    return (
        <div className="Login">
            <PermModal
                title="Login"
            >
                <DiscordButton />
            </PermModal>
        </div>
    );
}

export default Login;