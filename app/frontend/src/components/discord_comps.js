// src/Button.js
import React from 'react';
import '../css/theme.css';
import { DiscordAppID, ReturnDomain } from '../globals';


export const DiscordButton = () => {

    const return_url = `https://discord.com/api/oauth2/authorize?client_id=${DiscordAppID}&redirect_uri=${ReturnDomain}/callback&response_type=code&scope=identify`;

    return (
        <div className="flex justify-center"> {/* Added container with flexbox */}
            <a
                href={return_url}
                className="inline-block px-4 py-2 bg-indigo-700 text-white rounded hover:bg-indigo-800 transition duration-300"
            >
                Login with Discord
            </a>
        </div>
    );
};